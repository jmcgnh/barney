# Barney

Barney is a personal learning project to do Ansible provisioning of a Raspberry Pi running Raspbian Lite. 

#### Background

I started my first IoT project in January 2017: a garage door controller based on Electric Imp where I got my initial instructions and inspirations from a post by Aaron at `http://secretsciencelab.com/diy-electric-imp-wifi-garage-opener-sensor-for-under-50-weekend-project/` which worked _somewhat_ and I learned a lot.

Later that year, I learned of the ESP8266 and acquired some, with the first project a weather station reporting to Weather Underground.

I already had a Raspberry Pi by this time, but after trying it out as a very cheap desktop computer, it didn't inspire me to start any projects.

The Raspberry Pi Zero W, however, did stimulate some interest, since the price was right and the capabilities were a lot more than the other boards I had been working with: Imp, ESP8266, and various Arduinos.

My 'ambitious' vision was to have multiple ESP8266 weather stations, some battery or even solar powered, reporting back through a RPZW running MQTT and providing other services, including an OTA update capability for the ESP8266 device.

#### Next steps

The initial provisioning of the RPZW module is handled by some scripts at

   `https://gitlab.com/jmcgnh/pi-boot-script`
   
This gets the device onto WiFi with ssh enabled for public key authentication only and a distinct hostname.

From this point, I expect to use Ansible to do the rest of the setup.

* assign a hostname and role for the device `this is an interaction between the ansible inventory and something I have yet to set up`
* set up acme.sh to generate certificates `nickjj.acme_sh`
* set up DDNS so the device can be accessed from the internet `Can ansible reach out to do this for a Tomato router? Some other router?`
* set up haproxy, nginx `Trying jebovic.haproxy and jebovic.nginx`
* MQTT `pbonrad.mosquitto`
* (there's some question of whether I should adopt the Homie convention - `https://homieiot.github.io/`)
* set up the OTA support website (perhaps DANCER?)
* database and dashboard (InfluxDB, Grafana, nodeRED, other things under consideration)

#### Other resources

I read the README for `https://github.com/cogini/ansible-role-users` and found it made a certain amount of sense for organizing user roles.
